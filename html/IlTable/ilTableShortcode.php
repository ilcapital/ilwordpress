<?php

/**
 * Plugin Name: IlTable
 * Description: Plugin chargeant les librairies et fichiers js nécessaires à la définition et l'utilisation du shortcode il_insertTable servant à l'affichage de tableaux de données.
 * Version: Version 1.0.0
 * Author: Alexis Tajfel Intralinks
 */



class iltable_shortcodeAngular {
	static function init() {
		add_shortcode('il_insertTable', array(__CLASS__, 'iltable_shortcodeHandler'));
		add_action('init', array(__CLASS__, 'register_script'));
	}

	static function register_script() {
		wp_enqueue_script('angularlib', 'http://ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular.min.js' , array('jquery'), false);
		wp_enqueue_script('AngularJS', plugins_url("AngularJS.js", __FILE__), array('jquery'), false);
		//wp_enqueue_style('bootstrapCSS','http://maxcdn.bootstrapcdn.com/bootswatch/3.2.0/sandstone/bootstrap.min.css', array(), false);
		//wp_enqueue_style('bootstrapFontCSS','http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css', array(), false);
	}

	static function changecontroller() {
		static $i = 0;
		return $i;
		$i += 1;
	}

	static function createController() {
		?>
		<script>
			angular.module('sortApp', []).controller('mainController', function($scope, $http) {
				$scope.sortType     = 'owner'; // set the default sort type
				$scope.sortReverse  = false;  // set the default sort order
				$scope.searchDoc    = '';     // set the default search/filter term

				$http.get('http://ec2-52-29-213-62.eu-central-1.compute.amazonaws.com/wp-content/plugins/IlTable/docJSON.php').success(function(data, status, headers, config) {
				$scope.document = data["documents"];
				});
			});
		</script>
		<?php
	}

	static function iltable_shortcodeHandler() {
		return <<<CONTENT
		<div ng-app="sortApp" ng-controller="mainController{iltable_shortcodeAngular::changecontroller()}">
			<form>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-search"></i></div>
						<input type="text" class="form-control" placeholder="Type here to search a document" ng-model="searchDoc">
					</div>
				</div>
			</form>

			<table class="table table-bordered table-striped">
				<thead>
					<tr> 
						<td>
							<a href="#" ng-click="sortType = 'name'; sortReverse = !sortReverse">
							Names
							<span ng-show="sortType == 'name' && !sortReverse" class="fa fa-caret-down"></span>
							<span ng-show="sortType == 'name' && sortReverse" class="fa fa-caret-up"></span>
							</a>
						</td>
						<td>
							<a href="#" ng-click="sortType = 'fileHash'; sortReverse = !sortReverse">
							FileHash
							<span ng-show="sortType == 'fileHash' && !sortReverse" class="fa fa-caret-down"></span>
							<span ng-show="sortType == 'fileHash' && sortReverse" class="fa fa-caret-up"></span>
							</a>
						</td>
						<td>
							<a href="#" ng-click="sortType = 'owner'; sortReverse = !sortReverse">
							Owner 
							<span ng-show="sortType == 'owner' && !sortReverse" class="fa fa-caret-down"></span>
							<span ng-show="sortType == 'owner' && sortReverse" class="fa fa-caret-up"></span>
							</a>
						</td>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="doc in document | orderBy:sortType:sortReverse | filter:searchDoc">
						<td>{{ doc.name }}</td>
						<td>{{ doc.fileHash }}</td>
						<td>{{ doc.owner }}</td>
					</tr>
				</tbody>
			</table>
		</div>
CONTENT;
	}
}

iltable_shortcodeAngular::init();




?>