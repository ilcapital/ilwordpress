function bckcolor(nbelements) {
	var colorArray = []
	 if (nbelements < 6) {
		colorArray = [
		'rgba(255, 0, 127, 0.2)',
		'rgba(54, 162, 235, 0.2)',
		'rgba(255, 206, 86, 0.2)',
		'rgba(248, 142, 85, 0.2)',
		'rgba(22, 184, 78, 0.2)'
		]
	 }
	 else if (nbelements < 11) {
		colorArray = [
		'rgba(160, 240, 255, 0.5)',
		'rgba(136, 227, 244, 0.5)',
		'rgba(119, 198, 232, 0.5)',
		'rgba(102, 170, 221, 0.5)',
		'rgba(85, 142, 210, 0.5)',
		'rgba(68, 113, 198, 0.5)',
		'rgba(51, 85, 187, 0.5)',
		'rgba(34, 57, 176, 0.5)',
		'rgba(17, 28, 164, 0.5)',
		'rgba(0, 0, 153, 0.5)'
		]
	 }
	 else if (nbelements > 10) {
	 	colorArray =[]
	 }

	return colorArray;
}

function borderColor (nbelements) {
	var borderColorArray=[]
	 if (nbelements < 6) {
		borderColorArray = [
		'rgba(255, 0, 127, 1)',
		'rgba(54, 162, 235, 1)',
		'rgba(255, 206, 86, 1)',
		'rgba(248, 142, 85, 1)',
		'rgba(22, 184, 78, 1)'
		]
	}
	else if (nbelements < 11) {
		borderColorArray = [
		'rgba(160, 240, 255, 1)',
		'rgba(136, 227, 244, 1)',
		'rgba(119, 198, 232, 1)',
		'rgba(102, 170, 221, 1)',
		'rgba(85, 142, 210, 1)',
		'rgba(68, 113, 198, 1)',
		'rgba(51, 85, 187, 1)',
		'rgba(34, 57, 176, 1)',
		'rgba(17, 28, 164, 1)',
		'rgba(0, 0, 153, 1)'
		]
	}
	return borderColorArray;
}

function show_doughnut(canvasId, labels, data) {
	var ctx = document.getElementById(canvasId);
	var myChart = new Chart(ctx, {
		type: 'doughnut',
	    data: {
	        labels: labels,
	        datasets: [{
	            data: data,
	            backgroundColor: bckcolor(labels.length),
	            borderColor: borderColor(labels.length)
	        }]
	    },
	});
}

function show_pie(canvasId, labels, data) {
	var ctx = document.getElementById(canvasId);
	var myChart = new Chart(ctx, {
		type: 'pie',
	    data: {
	        labels: labels,
	        datasets: [{
	            data: data,
	            backgroundColor: bckcolor(labels.length),
	            borderColor: borderColor(labels.length)
	        }]
	    },
	});
}

function show_radar(canvasId, labels, data) {
	var ctx = document.getElementById(canvasId);
	var myChart = new Chart(ctx, {
		type: 'radar',
	    data: {
	        labels: labels,
	        datasets: [{
	            data: data,
	            backgroundColor: bckcolor(labels.length),
	            borderColor: borderColor(labels.length)
	        }]
	    },
	});
}

function show_bar(canvasId, labels, data) {
	var ctx = document.getElementById(canvasId);
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				label: 'effectif',
				data: data,
				backgroundColor: 'rgba(85, 142, 210, 0.5)',
				}, {
				label:'effectif bis',
				backgroundColor: 'rgba(0, 230, 180, 0.5)',
				data: data,
			}]
		},
	options: {
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}]
		}
	}
});
}

function show_line(canvasId, labels, data) {
	var ctx = document.getElementById(canvasId);
	var myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: labels,
			datasets: [{
				data: data,
				backgroundColor: bckcolor(labels.length),
				borderColor: borderColor(labels.length)
			}]
		},
	options: {
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}]
		}
	}
});
}
