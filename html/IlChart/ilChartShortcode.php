<?php


/**
 * Plugin Name: IlChart
 * Description: Plugin chargeant les librairies et fichiers js nécessaires à la définition et l'utilisation du shortcode il_insertchart servant à l'affichage de charts.
 * Version: Version 1.0.0
 * Author: Alexis Tajfel Intralinks
 */

class ilchart_loadsJavascriptForShortcode {
	static function init() {
		add_shortcode('il_insertChart', array(__CLASS__, 'ilchart_shortcode_adapt'));
		add_action('init', array(__CLASS__, 'enqueue_script'));
	}

	static function ilchart_insert_doughnut($chartid, $dataAllString, $chartType) {
		$dataAllarray    = explode( "|", $dataAllString );
		$dataLabelsArray = [];
		$dataValuesArray = [];

		$errorFound = 0;

		for ($i = 0; $i < count($dataAllarray); $i++) {
			if ($i%2 == 0){
				$dataLabelsArray[] =/*stripTags*/ $dataAllarray[$i];
			}
			else {
				if (is_numeric($dataAllarray[$i])) {
					$errorFound =$errorFound + 0;
					$dataValuesArray[] = (float)$dataAllarray[$i];
				}
				else {
					$errorFound = $errorFound + 1;
				}
			}
		}
		$dataLabelsString = "[\"" . implode("\",\"", $dataLabelsArray) . "\"]";
		$dataValuesString = "[" . implode(",", $dataValuesArray) . "]";
		
		$mapping = ['pie' => 'show_pie', 'doughnut' => 'show_doughnut', 'bar' => 'show_bar', 'line' => 'show_line', 'radar' => 'show_radar', 'stack' => 'show_stack'];
		if ($errorFound > 0) {
			return <<<CONTENT
			<p> System founds wrong labels or values, please check the input. </p>
CONTENT;
		}
		else {
			return <<<CONTENT
			<canvas id="$chartid" height="80px"></canvas>
			<script type="text/javascript">
			{$mapping[$chartType]}("$chartid", $dataLabelsString, $dataValuesString);
			</script>
CONTENT;
		}
	}

	static function ilchart_shortcode_adapt($shortcodeArray) {
		$randomId = (string) rand(0, 1000000000);
		$stringFromArray = $shortcodeArray['data'];
		$chartType = (string)$shortcodeArray['type'];
		return ilchart_loadsJavascriptForShortcode::ilchart_insert_doughnut($randomId, $stringFromArray, $chartType);
	}

	static function enqueue_script() {
		wp_enqueue_script('chartsFunctions', plugins_url("chartsFunctions.js", __FILE__), array('jquery'), false);
		wp_enqueue_script('chartlib', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.min.js' , array('jquery'), false);
	}
}

ilchart_loadsJavascriptForShortcode::init();

?>