<?php

function load_content_without_curl($url)
	{
		$opts = array
			(
			'http' => array
				(
					'method'  => 'POST',
					'header'  => "Content-Type: text/xml",
					'content' => $body,
					'timeout' => 60
				)
			);

		$context = stream_context_create($opts);
		$content = file_get_contents($url, true, $context);
		return $content;
	}

function load_content_with_curl($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$content = curl_exec($ch);

		curl_close($ch);
		return $content;
	}

function load_content($url)
	{
		return load_content_with_curl($url);
	}

function execute($url){
	
	$content = load_content("http://ec2-52-58-218-100.eu-central-1.compute.amazonaws.com/testjson/all.xml");
	$stringParse = simplexml_load_string($content);

	class DocumentObject 
	{
		public $name;
		public $creator;
		public $protected;
	}

	$documents = array();

	for ( $i=0 ; $i<((int)$stringParse->pagingContext->totalResults) ; $i++ ) {
		$node = $stringParse->contentList->document->$i;
		$doc = new DocumentObject;
		$doc->name = $node->name;
		$doc->creator = $node->createdBy->firstName . " " . $node->createdBy->lastName;
		$doc->protected = $node->isIrmSecured;

		$documents[] = $doc;
	}

	return $documents;
}



?>

<!DOCTYPE HTML>
<html>
<head>
<title>Le Test</title>
</head>

<body>


<hr>
<hr>


<table border = "1">
	<tr>
		<th>Name</th>
		<th>Creator</th>
		<th>Is protected ?</th>
	</tr>
<?php $documents = execute("http://ec2-52-58-218-100.eu-central-1.compute.amazonaws.com/testjson/all.xml");
	foreach ($documents as $doc): ?>

	<tr>
		<td><?php echo $doc->name; ?></td>
		<td><?php echo $doc->creator; ?></td>
		<td><?php echo $doc->protected; ?></td>
	</tr>	
<?php endforeach ?>

</table>

</body>
</html>

